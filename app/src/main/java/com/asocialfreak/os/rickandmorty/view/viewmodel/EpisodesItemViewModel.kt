package com.asocialfreak.os.rickandmorty.view.viewmodel

import android.content.Context
import android.content.Intent
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asocialfreak.os.rickandmorty.view.EpisodeDetailActivity
import com.asocialfreak.os.rickandmorty.view.KEY_INTENT_EPISODE
import com.asocialfreak.os.rickandmorty.model.data.Episode

class EpisodesItemViewModel : ViewModel() {

    val liveEpisode = LiveEpisode()
    var episode: Episode? = null
        private set

    fun bindEpisode(episode: Episode) {
        this.episode = episode

        liveEpisode.apply {
            name.value = episode.name
            countOfCharacters.value = episode.characters.size
            episodeOrder.value = episode.episode
            airDate.value = episode.air_date.toLowerCase()
        }
    }

    fun onItemClicked(context: Context, episode: Episode?) {
        if (episode != null) {
            val intent = Intent(context, EpisodeDetailActivity::class.java)
            intent.putExtra(KEY_INTENT_EPISODE, episode)
            context.startActivity(intent)
        }
    }

    inner class LiveEpisode {
        val name = MutableLiveData<String>()
        val countOfCharacters = MutableLiveData<Int>()
        val episodeOrder = MutableLiveData<String>()
        val airDate = MutableLiveData<String>()
    }

}
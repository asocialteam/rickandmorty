package com.asocialfreak.os.rickandmorty.api

import com.asocialfreak.os.rickandmorty.BuildConfig
import com.asocialfreak.os.rickandmorty.api.service.CharacterService
import com.asocialfreak.os.rickandmorty.api.service.EpisodeService
import com.asocialfreak.os.rickandmorty.api.service.LocationService
import com.asocialfreak.os.rickandmorty.model.ResponseException
import com.asocialfreak.os.rickandmorty.model.data.Character
import com.asocialfreak.os.rickandmorty.model.data.Episode
import com.jakewharton.retrofit2.adapter.kotlin.coroutines.CoroutineCallAdapterFactory
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.converter.moshi.MoshiConverterFactory

object ApiManager {

    private val retrofit by lazy {
        val logging = HttpLoggingInterceptor()
        logging.level = HttpLoggingInterceptor.Level.BASIC
        val client = OkHttpClient.Builder()
            .addInterceptor(logging)
            .build()

        Retrofit.Builder()
            .client(client)
            .baseUrl(BuildConfig.BASE_URL)
            .addConverterFactory(MoshiConverterFactory.create())
            .addCallAdapterFactory(CoroutineCallAdapterFactory())
            .build()
    }

    private val episodeService: EpisodeService by lazy {
        retrofit.create(EpisodeService::class.java)
    }

    private val locationService: LocationService by lazy {
        retrofit.create(LocationService::class.java)
    }

    private val characterService: CharacterService by lazy {
        retrofit.create(CharacterService::class.java)
    }

    //region Episode
    suspend fun loadFirstPageOfEpisodes() = episodeService.loadEpisodesAsync().await()
    suspend fun loadNextPageOfEpisodes(url: String) = episodeService.loadNextPageAsync(url).await()

    @Throws(ResponseException::class)
    suspend fun loadEpisodesByIds(episodeIds: Collection<String>): Array<Episode> {
        val joinedIds = episodeIds.joinToString(",")

         return if (episodeIds.size > 1) {
            val response = episodeService.loadEpisodesByIdsAsync(joinedIds).await()
            if (response.isSuccessful) {
                response.body() ?: throw ResponseException(response.message())
            } else throw ResponseException(response.message())
        } else {
            val response = episodeService.loadEpisodeByIdAsync(joinedIds).await()
            if (response.isSuccessful) {
                arrayOf(response.body() ?: throw ResponseException(response.message()))
            } else throw ResponseException(response.message())
        }
    }
    //endregion Episode

    //region Location
    suspend fun loadFirstPageOfLocations() = locationService.loadLocationsAsync().await()
    suspend fun loadNextPageOfLocations(url: String) = locationService.loadNextPageAsync(url).await()
    //endregion Location

    //region Character
    suspend fun loadFirstPageOfCharacters() = characterService.loadCharacterAsync().await()
    suspend fun loadNextPageOfCharacters(url: String) = characterService.loadNextPageAsync(url).await()

    @Throws(ResponseException::class)
    suspend fun loadCharactersByIds(characterIds: Collection<String>): Array<Character> {
        val joinedIds = characterIds.joinToString(",")

        return if (characterIds.size > 1) {
            val response = characterService.loadCharactersByIdsAsync(joinedIds).await()
            if (response.isSuccessful) {
                response.body() ?: throw ResponseException(response.message())
            } else throw ResponseException(response.message())
        } else {
            val response = characterService.loadCharacterByIdAsync(joinedIds).await()
            if (response.isSuccessful) {
                arrayOf(response.body() ?: throw ResponseException(response.message()))
            } else throw ResponseException(response.message())
        }
    }
    //endregion Character

}
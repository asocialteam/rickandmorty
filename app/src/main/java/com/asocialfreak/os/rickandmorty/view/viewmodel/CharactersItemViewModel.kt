package com.asocialfreak.os.rickandmorty.view.viewmodel

import android.content.Context
import android.content.Intent
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asocialfreak.os.rickandmorty.view.CharacterDetailActivity
import com.asocialfreak.os.rickandmorty.view.KEY_INTENT_CHARACTER
import com.asocialfreak.os.rickandmorty.model.data.Character

class CharactersItemViewModel: ViewModel() {

    val liveCharacter = LiveCharacter()
    var character: Character? = null
        private set

    fun bindCharacter(character: Character){
        this.character = character

        liveCharacter.apply {
            name.value = character.name
            location.value = character.location?.name
            countOfEpisodes.value = character.episode.size
            imageUrl.value = character.image
            status.value = character.status
        }
    }

    fun onItemClicked(context: Context, character: Character?){
        if(character != null) {
            val intent = Intent(context, CharacterDetailActivity::class.java)
            intent.putExtra(KEY_INTENT_CHARACTER, character)
            context.startActivity(intent)
        }
    }

    inner class LiveCharacter {
        val name = MutableLiveData<String>()
        val location = MutableLiveData<String>()
        val countOfEpisodes = MutableLiveData<Int>()
        val imageUrl = MutableLiveData<String>()
        val status = MutableLiveData<String>()
    }

}
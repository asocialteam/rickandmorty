package com.asocialfreak.os.rickandmorty.view.adapter

import android.content.Context
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentPagerAdapter
import com.asocialfreak.os.rickandmorty.R
import com.asocialfreak.os.rickandmorty.view.fragment.CharactersFragment
import com.asocialfreak.os.rickandmorty.view.fragment.EpisodesFragment
import com.asocialfreak.os.rickandmorty.view.fragment.LocationsFragment
import com.asocialfreak.os.rickandmorty.view.viewmodel.EpisodesFragmentViewModel


private enum class TAB(val titleResId: Int, val fragmentClass: Class<out Fragment>) {
    CHARACTERS(R.string.activity_main_tab_characters, CharactersFragment::class.java),
    EPISODES(R.string.activity_main_tab_episodes, EpisodesFragment::class.java),
    LOCATIONS(R.string.activity_main_tab_locations, LocationsFragment::class.java),
}

class MainPagerAdapter(private val context: Context, fragmentManager: FragmentManager) :
    FragmentPagerAdapter(fragmentManager) {

    private val tabs = TAB.values()
    private val fragments = arrayOfNulls<Fragment>(tabs.size)

    override fun getItem(position: Int): Fragment {
        if(fragments[position] == null){
            fragments[position] = tabs[position].fragmentClass.newInstance()
        }
        return fragments[position]!!
    }


    override fun getPageTitle(position: Int): String {
        return context.resources.getString(tabs[position].titleResId)
    }

    override fun getCount() = tabs.size
}
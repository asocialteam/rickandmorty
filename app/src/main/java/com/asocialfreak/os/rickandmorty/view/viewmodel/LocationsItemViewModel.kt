package com.asocialfreak.os.rickandmorty.view.viewmodel

import android.content.Context
import android.content.Intent
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asocialfreak.os.rickandmorty.model.data.Location
import com.asocialfreak.os.rickandmorty.view.KEY_INTENT_LOCATION
import com.asocialfreak.os.rickandmorty.view.LocationDetailActivity

class LocationsItemViewModel: ViewModel() {

    val liveLocation = LiveLocation()
    var location: Location? = null
        private set

    fun bindLocation(location: Location){
        this.location = location

        liveLocation.apply {
            name.value = location.name
            countOfResidents.value = location.residents.size
            type.value = location.type
            dimension.value = location.dimension
        }
    }

    fun onItemClicked(context: Context, location: Location?){
        if(location != null) {
            val intent = Intent(context, LocationDetailActivity::class.java)
            intent.putExtra(KEY_INTENT_LOCATION, location)
            context.startActivity(intent)
        }
    }

    inner class LiveLocation {
        val name = MutableLiveData<String>()
        val countOfResidents = MutableLiveData<Int>()
        val type = MutableLiveData<String>()
        val dimension = MutableLiveData<String>()
    }

}
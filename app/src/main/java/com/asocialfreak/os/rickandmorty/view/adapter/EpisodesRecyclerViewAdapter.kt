package com.asocialfreak.os.rickandmorty.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.recyclerview.widget.RecyclerView
import com.asocialfreak.os.rickandmorty.databinding.FragmentEpisodesItemBinding
import com.asocialfreak.os.rickandmorty.model.data.Episode
import com.asocialfreak.os.rickandmorty.view.viewmodel.EpisodesItemViewModel

class EpisodesRecyclerViewAdapter : RecyclerView.Adapter<EpisodesRecyclerViewAdapter.ViewHolder>() {

    private var episodes: MutableList<Episode> = mutableListOf()

    fun addData(episodes: Array<Episode>) {
        this.episodes.addAll(episodes)
        notifyItemRangeInserted(
            this.episodes.size - episodes.size,
            episodes.size
        )
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        LayoutInflater.from(parent.context).run {
            FragmentEpisodesItemBinding.inflate(this, parent, false).let { binding ->
                val viewHolder = ViewHolder(binding.root)
                binding.viewModel = viewHolder.viewModel
                binding.lifecycleOwner = viewHolder
                viewHolder
            }
        }

    override fun getItemCount() = episodes.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(episodes[position])

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view), LifecycleOwner {

        val viewModel = EpisodesItemViewModel()

        private val lifecycleRegistry = LifecycleRegistry(this)

        init {
            lifecycleRegistry.markState(Lifecycle.State.STARTED)
        }

        fun bind(episode: Episode) {
            viewModel.bindEpisode(episode)
        }

        override fun getLifecycle() = lifecycleRegistry
    }
}
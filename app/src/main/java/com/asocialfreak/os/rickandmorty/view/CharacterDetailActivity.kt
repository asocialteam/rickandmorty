package com.asocialfreak.os.rickandmorty.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.asocialfreak.os.rickandmorty.R
import com.asocialfreak.os.rickandmorty.databinding.ActivityCharacterDetailBinding
import com.asocialfreak.os.rickandmorty.view.viewmodel.CharacterDetailActivityViewModel

const val KEY_INTENT_CHARACTER = "key_character"

class CharacterDetailActivity : AppCompatActivity() {

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(CharacterDetailActivityViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onCreate(intent)
        DataBindingUtil.setContentView<ActivityCharacterDetailBinding>(
            this,
            R.layout.activity_character_detail
        ).also { binding ->
            binding.lifecycleOwner = this
            binding.viewModel = viewModel
        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.onStart()
    }
}

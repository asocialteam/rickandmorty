package com.asocialfreak.os.rickandmorty.view.viewmodel

import android.content.Intent
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asocialfreak.os.rickandmorty.view.KEY_INTENT_LOCATION
import com.asocialfreak.os.rickandmorty.api.ApiManager
import com.asocialfreak.os.rickandmorty.model.ResponseException
import com.asocialfreak.os.rickandmorty.model.data.Location
import com.asocialfreak.os.rickandmorty.view.adapter.CharactersRecyclerViewAdapter
import kotlinx.coroutines.*

class LocationDetailActivityViewModel : ViewModel() {

    companion object {
        private const val TAG = "LocationDetailAcVM"
    }

    private val parentJob = Job()
    private val viewModelScope = CoroutineScope(parentJob + Dispatchers.Default)
    private val errorHandler
        get() = CoroutineExceptionHandler { _, e ->
            when (e) {
                is ResponseException -> Log.e(TAG, "Error while processing a response", e)
                else -> Log.e(TAG, "General error occurred", e)
            }
            areLocationsLoaded = false
        }

    private var areLocationsLoaded = false
    private var location: Location? = null

    val liveLocation = LiveLocation()
    val adapter = CharactersRecyclerViewAdapter()

    fun onCreate(intent: Intent) {
        if (this.location == null) {
            this.location = intent.getParcelableExtra(KEY_INTENT_LOCATION) ?: return
            liveLocation.apply {
                name.value = location!!.name
                type.value = location!!.type
                dimension.value = location!!.dimension
            }
        }
    }

    fun onStart() {
        if (!areLocationsLoaded && location != null) {
            areLocationsLoaded = true
            loadCharacters(location!!.residents)
        }
    }

    private fun loadCharacters(characterUrls: Array<String>) {
        if (characterUrls.isNotEmpty()) {
            viewModelScope.launch(errorHandler) {
                val characterIds = characterUrls.map { episode ->
                    episode.split('/').last()
                }

                val characters = ApiManager.loadCharactersByIds(characterIds)
                adapter.addData(characters)
            }
        }
    }

    inner class LiveLocation {
        val name = MutableLiveData<String>()
        val type = MutableLiveData<String>()
        val dimension = MutableLiveData<String>()
    }

}
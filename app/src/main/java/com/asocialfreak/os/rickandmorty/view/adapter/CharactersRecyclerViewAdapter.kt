package com.asocialfreak.os.rickandmorty.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.recyclerview.widget.RecyclerView
import com.asocialfreak.os.rickandmorty.databinding.FragmentCharactersItemBinding
import com.asocialfreak.os.rickandmorty.model.data.Character
import com.asocialfreak.os.rickandmorty.view.viewmodel.CharactersItemViewModel


class CharactersRecyclerViewAdapter : RecyclerView.Adapter<CharactersRecyclerViewAdapter.ViewHolder>() {

    private val characters: MutableList<Character> = mutableListOf()

    fun addData(characters: Array<Character>){
        this.characters.addAll(characters)
        notifyItemRangeInserted(
            this.characters.size - characters.size,
            characters.size
        )
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        LayoutInflater.from(parent.context).run {
            FragmentCharactersItemBinding.inflate(this, parent, false)
                .let { binding ->
                    val viewHolder = ViewHolder(binding.root)
                    binding.viewModel = viewHolder.viewModel
                    binding.lifecycleOwner = viewHolder
                    viewHolder
                }
        }

    override fun getItemCount() = characters.size
    override fun onBindViewHolder(holder: ViewHolder, position: Int) = holder.bind(characters[position])

    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view), LifecycleOwner {

        val viewModel = CharactersItemViewModel()

        private val lifecycleRegistry = LifecycleRegistry(this)

        init {
            lifecycleRegistry.markState(Lifecycle.State.STARTED)
        }

        fun bind(character: Character) {
            viewModel.bindCharacter(character)
        }

        override fun getLifecycle() = lifecycleRegistry
    }
}
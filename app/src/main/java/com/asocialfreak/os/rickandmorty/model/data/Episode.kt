package com.asocialfreak.os.rickandmorty.model.data

import android.os.Parcel
import android.os.Parcelable

class Episode() : Parcelable {
    var id: Long = 0
    var name: String = ""
    var air_date: String = ""
    var episode: String = ""
    var characters: Array<String> = emptyArray()

    constructor(parcel: Parcel) : this() {
        id = parcel.readLong()
        name = parcel.readString() ?: ""
        air_date = parcel.readString() ?: ""
        episode = parcel.readString() ?: ""
        characters = parcel.createStringArray() ?: emptyArray()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(name)
        parcel.writeString(air_date)
        parcel.writeString(episode)
        parcel.writeStringArray(characters)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Episode> {
        override fun createFromParcel(parcel: Parcel): Episode {
            return Episode(parcel)
        }

        override fun newArray(size: Int): Array<Episode?> {
            return arrayOfNulls(size)
        }
    }
}
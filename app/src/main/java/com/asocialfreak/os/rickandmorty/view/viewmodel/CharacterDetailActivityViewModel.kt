package com.asocialfreak.os.rickandmorty.view.viewmodel

import android.content.Intent
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asocialfreak.os.rickandmorty.view.KEY_INTENT_CHARACTER
import com.asocialfreak.os.rickandmorty.api.ApiManager
import com.asocialfreak.os.rickandmorty.model.ResponseException
import com.asocialfreak.os.rickandmorty.model.data.Character
import com.asocialfreak.os.rickandmorty.view.adapter.EpisodesRecyclerViewAdapter
import kotlinx.coroutines.*

class CharacterDetailActivityViewModel : ViewModel() {

    companion object {
        private const val TAG = "CharacterDetailAcVM"
    }

    private val parentJob = Job()
    private val viewModelScope = CoroutineScope(parentJob + Dispatchers.Default)
    private val errorHandler
        get() = CoroutineExceptionHandler { _, e ->
            when (e) {
                is ResponseException -> Log.e(TAG, "Error while processing a response", e)
                else -> Log.e(TAG, "General error occurred", e)
            }
            areEpisodesLoaded = false
        }

    private var areEpisodesLoaded = false
    private var character: Character? = null

    val liveCharacter = LiveCharacter()
    val adapter = EpisodesRecyclerViewAdapter()

    fun onCreate(intent: Intent) {
        if (character == null) {
            character = intent.getParcelableExtra<Character?>(KEY_INTENT_CHARACTER) ?: return
            liveCharacter.apply {
                name.value = character!!.name
                gender.value = character!!.gender
                species.value = character!!.species
                status.value = character!!.status
                type.value = character!!.type
                imageUrl.value = character!!.image
                originName.value = character!!.origin?.name
                locationName.value = character!!.location?.name
                isTypeEmpty.value = character!!.type.isBlank()
            }
        }
    }

    fun onStart() {
        if (!areEpisodesLoaded && character != null) {
            areEpisodesLoaded = true
            loadEpisodes(character!!.episode)
        }
    }

    private fun loadEpisodes(episodeUrls: Array<String>) {
        if (episodeUrls.isNotEmpty()) {
            viewModelScope.launch(errorHandler) {
                val episodeIds = episodeUrls.map { episode ->
                    episode.split('/').last()
                }

                val episodes = ApiManager.loadEpisodesByIds(episodeIds)
                adapter.addData(episodes)
            }
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.coroutineContext.cancelChildren()
    }

    inner class LiveCharacter {
        val name = MutableLiveData<String>()
        val originName = MutableLiveData<String>()
        val locationName = MutableLiveData<String>()
        val type = MutableLiveData<String>()
        val species = MutableLiveData<String>()
        val gender = MutableLiveData<String>()
        val status = MutableLiveData<String>()
        val imageUrl = MutableLiveData<String>()
        val isTypeEmpty = MutableLiveData<Boolean>()
    }
}
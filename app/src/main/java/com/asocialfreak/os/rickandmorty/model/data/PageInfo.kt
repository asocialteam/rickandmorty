package com.asocialfreak.os.rickandmorty.model.data

class PageInfo {
    var next: String = ""
    var prev: String = ""
}
package com.asocialfreak.os.rickandmorty.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.asocialfreak.os.rickandmorty.databinding.FragmentEpisodesBinding
import com.asocialfreak.os.rickandmorty.view.viewmodel.EpisodesFragmentViewModel

class EpisodesFragment : Fragment() {

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(EpisodesFragmentViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        FragmentEpisodesBinding.inflate(inflater, container, false).let { binding ->
            binding.lifecycleOwner = this
            binding.viewModel = viewModel
            binding.root
        }

    override fun onStart() {
        super.onStart()
        viewModel.onStart()
    }
}
package com.asocialfreak.os.rickandmorty.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.asocialfreak.os.rickandmorty.R
import com.asocialfreak.os.rickandmorty.databinding.ActivityEpisodeDetailBinding
import com.asocialfreak.os.rickandmorty.view.viewmodel.EpisodeDetailActivityViewModel

const val KEY_INTENT_EPISODE = "key_episode"

class EpisodeDetailActivity : AppCompatActivity() {

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(EpisodeDetailActivityViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onCreate(intent)
        DataBindingUtil.setContentView<ActivityEpisodeDetailBinding>(
            this,
            R.layout.activity_episode_detail
        ).also { binding ->
            binding.viewModel = viewModel
        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.onStart()
    }

}

package com.asocialfreak.os.rickandmorty.listener

import android.nfc.tech.MifareUltralight.PAGE_SIZE
import androidx.recyclerview.widget.LinearLayoutManager
import androidx.recyclerview.widget.RecyclerView

class OnRecyclerViewScrollListener(private val nextPageLoadingListener: OnNextPageLoadingListener) : RecyclerView.OnScrollListener() {

    override fun onScrolled(recyclerView: RecyclerView, dx: Int, dy: Int) {
        super.onScrolled(recyclerView, dx, dy)

        val layoutManager = recyclerView.layoutManager as? LinearLayoutManager
            ?: throw RuntimeException("Only LinearLayoutManager is supported")
        val visibleItemCount = layoutManager.childCount
        val totalItemCount = layoutManager.itemCount
        val firstVisibleItemPosition = layoutManager.findFirstVisibleItemPosition()

        if (!nextPageLoadingListener.isNextPageLoading()
            && nextPageLoadingListener.isNextPageExists()) {
            if ((visibleItemCount + firstVisibleItemPosition) >= totalItemCount
                && firstVisibleItemPosition >= 0
                && totalItemCount >= PAGE_SIZE
            ) {
                nextPageLoadingListener.loadNextPage()
            }
        }

    }
}
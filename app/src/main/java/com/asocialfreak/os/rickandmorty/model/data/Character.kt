package com.asocialfreak.os.rickandmorty.model.data

import android.os.Parcel
import android.os.Parcelable

class Character() : Parcelable{
    var id: Long = 0
    var name: String = ""
    var status: String = ""
    var species: String = ""
    var type: String = ""
    var gender: String = ""
    var image: String = ""
    var origin: Location? = null
    var location: Location? = null
    var episode: Array<String> = emptyArray()

    constructor(parcel: Parcel) : this() {
        id = parcel.readLong()
        name = parcel.readString() ?: ""
        status = parcel.readString() ?: ""
        species = parcel.readString() ?: ""
        type = parcel.readString() ?: ""
        gender = parcel.readString() ?: ""
        image = parcel.readString() ?: ""
        origin = parcel.readParcelable(Location::class.java.classLoader)
        location = parcel.readParcelable(Location::class.java.classLoader)
        episode = parcel.createStringArray() ?: emptyArray()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(name)
        parcel.writeString(status)
        parcel.writeString(species)
        parcel.writeString(type)
        parcel.writeString(gender)
        parcel.writeString(image)
        parcel.writeParcelable(origin, flags)
        parcel.writeParcelable(location, flags)
        parcel.writeStringArray(episode)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Character> {
        override fun createFromParcel(parcel: Parcel): Character {
            return Character(parcel)
        }

        override fun newArray(size: Int): Array<Character?> {
            return arrayOfNulls(size)
        }
    }

}
package com.asocialfreak.os.rickandmorty.api.service

import com.asocialfreak.os.rickandmorty.model.LocationResponseBody
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Url

interface LocationService {

    @GET("location")
    fun loadLocationsAsync(): Deferred<Response<LocationResponseBody>>

    @GET
    fun loadNextPageAsync(@Url url: String): Deferred<Response<LocationResponseBody>>
}
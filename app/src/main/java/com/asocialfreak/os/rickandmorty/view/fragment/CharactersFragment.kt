package com.asocialfreak.os.rickandmorty.view.fragment

import android.os.Bundle
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProviders
import com.asocialfreak.os.rickandmorty.databinding.FragmentCharactersBinding
import com.asocialfreak.os.rickandmorty.view.viewmodel.CharactersFragmentViewModel

class CharactersFragment : Fragment() {

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(CharactersFragmentViewModel::class.java)
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?) =
        FragmentCharactersBinding.inflate(inflater, container, false).let { binding ->
            binding.lifecycleOwner = this
            binding.viewModel = viewModel
            binding.root
        }

    override fun onStart() {
        super.onStart()
        viewModel.onStart()
    }
}
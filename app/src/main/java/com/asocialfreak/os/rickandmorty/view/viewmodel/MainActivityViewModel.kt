package com.asocialfreak.os.rickandmorty.view.viewmodel

import android.content.Context
import androidx.databinding.ObservableField
import androidx.fragment.app.FragmentManager
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asocialfreak.os.rickandmorty.view.adapter.MainPagerAdapter

class MainActivityViewModel : ViewModel() {

    var adapter = MutableLiveData<MainPagerAdapter>()

    fun onCreate(context: Context, fragmentManager: FragmentManager) {
        adapter.apply {
            value = MainPagerAdapter(context, fragmentManager)
        }
    }
}
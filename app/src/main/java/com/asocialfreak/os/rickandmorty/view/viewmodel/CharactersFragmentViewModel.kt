package com.asocialfreak.os.rickandmorty.view.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asocialfreak.os.rickandmorty.api.ApiManager
import com.asocialfreak.os.rickandmorty.listener.OnNextPageLoadingListener
import com.asocialfreak.os.rickandmorty.listener.OnRecyclerViewScrollListener
import com.asocialfreak.os.rickandmorty.model.data.PageInfo
import com.asocialfreak.os.rickandmorty.model.CharacterResponseBody
import com.asocialfreak.os.rickandmorty.model.ResponseException
import com.asocialfreak.os.rickandmorty.model.data.Character
import com.asocialfreak.os.rickandmorty.view.adapter.CharactersRecyclerViewAdapter
import kotlinx.coroutines.*
import retrofit2.Response

class CharactersFragmentViewModel : ViewModel(), OnNextPageLoadingListener {

    companion object {
        private const val TAG = "CharactersFragmentVM"
    }

    private var parentJob = Job()
    private var viewModelScope = CoroutineScope(parentJob + Dispatchers.Main)
    private val errorHandler
        get() = CoroutineExceptionHandler { _, e ->
            when (e) {
                is ResponseException -> Log.e(TAG, "Error while processing a response", e)
                else -> Log.e(TAG, "General error occurred", e)
            }
        }

    private var pageInfo: PageInfo? = null
    private var isPageLoading = false

    val adapter = CharactersRecyclerViewAdapter()
    val onRecyclerViewScrollListener = OnRecyclerViewScrollListener(this)
    val isProgressVisible = MutableLiveData<Boolean>().apply {
        value = true
    }

    fun onStart() {
        if (pageInfo == null) {
            loadNextPage()
        }
    }

    override fun onCleared() {
        super.onCleared()
        viewModelScope.coroutineContext.cancelChildren()
    }

    override fun isNextPageExists() = pageInfo?.next?.isNotBlank() ?: false

    override fun isNextPageLoading() = isPageLoading

    override fun loadNextPage() {
        if (!isPageLoading) {
            isPageLoading = true
            viewModelScope.launch(errorHandler) {
                try {
                    val response = if (pageInfo == null) {
                        ApiManager.loadFirstPageOfCharacters()
                    } else {
                        ApiManager.loadNextPageOfCharacters(pageInfo!!.next)
                    }
                    postDataToRecyclerView(response)
                } finally {
                    isPageLoading = false
                    isProgressVisible.postValue(false)
                }
            }
        }
    }

    @Throws(ResponseException::class)
    private fun postDataToRecyclerView(response: Response<CharacterResponseBody>) {
        val responseBody = response.body()
        if (response.isSuccessful) {
            pageInfo = responseBody?.info ?: throw ResponseException(response.message())
            val characters = responseBody.results
            adapter.addData(characters)
        } else throw ResponseException(response.message())
    }

}
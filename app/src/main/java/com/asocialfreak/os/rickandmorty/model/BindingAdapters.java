package com.asocialfreak.os.rickandmorty.model;

import android.view.View;
import android.widget.ImageView;
import androidx.databinding.BindingAdapter;
import com.asocialfreak.os.rickandmorty.R;
import com.squareup.picasso.Picasso;

public class BindingAdapters {

    @BindingAdapter("app:statusIcon")
    public static void setStatusIcon(ImageView imageView, String status) {
        if (status == null) {
            imageView.setVisibility(View.GONE);
            return;
        }

        status = status.toLowerCase();

        if(status.equals("alive")){
            imageView.setVisibility(View.GONE);
            return;
        }

        imageView.setVisibility(View.VISIBLE);

        if (status.equals("unknown")) {
            imageView.setImageResource(R.drawable.ic_unknown);
        } else if (status.equals("dead")) {
            imageView.setImageResource(R.drawable.ic_skull);
        }
    }

    @BindingAdapter("app:compressedImage")
    public static void setCompressedImage(ImageView imageView, String url) {
        if (url != null && !url.isEmpty()) {
            Picasso.get().load(url)
                    .placeholder(R.drawable.ic_placeholder)
                    .resize(100, 100)
                    .onlyScaleDown()
                    .into(imageView);
        } else {
            Picasso.get().load(R.drawable.ic_placeholder).into(imageView);
        }
    }

    @BindingAdapter("app:image")
    public static void setImage(ImageView imageView, String url) {
        if (url != null && !url.isEmpty()) {
            Picasso.get().load(url)
                    .placeholder(R.drawable.ic_placeholder)
                    .into(imageView);
        } else {
            Picasso.get().load(R.drawable.ic_placeholder).into(imageView);
        }
    }
}

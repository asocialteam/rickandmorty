package com.asocialfreak.os.rickandmorty.model

import com.asocialfreak.os.rickandmorty.model.data.Character
import com.asocialfreak.os.rickandmorty.model.data.Episode
import com.asocialfreak.os.rickandmorty.model.data.PageInfo
import com.asocialfreak.os.rickandmorty.model.data.Location

data class LocationResponseBody(val info: PageInfo, val results: Array<Location>)

data class EpisodeResponseBody(val info: PageInfo, val results: Array<Episode>)

data class CharacterResponseBody(val info: PageInfo, val results: Array<Character>)
package com.asocialfreak.os.rickandmorty.view.viewmodel

import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asocialfreak.os.rickandmorty.api.ApiManager
import com.asocialfreak.os.rickandmorty.listener.OnNextPageLoadingListener
import com.asocialfreak.os.rickandmorty.listener.OnRecyclerViewScrollListener
import com.asocialfreak.os.rickandmorty.model.data.PageInfo
import com.asocialfreak.os.rickandmorty.model.LocationResponseBody
import com.asocialfreak.os.rickandmorty.model.ResponseException
import com.asocialfreak.os.rickandmorty.view.adapter.LocationsRecyclerViewAdapter
import kotlinx.coroutines.*
import retrofit2.Response

class LocationsFragmentViewModel : ViewModel(), OnNextPageLoadingListener {

    companion object {
        private const val TAG = "LocationsFragmentVM"
    }

    private var parentJob = Job()
    private var viewModelScope = CoroutineScope(parentJob + Dispatchers.Main)
    private val errorHandler
        get() = CoroutineExceptionHandler { _, e ->
            when (e) {
                is ResponseException -> Log.e(TAG, "Error while processing a response", e)
                else -> Log.e(TAG, "General error occurred", e)
            }
        }

    private var pageInfo: PageInfo? = null
    private var isPageLoading = false

    val adapter = LocationsRecyclerViewAdapter()
    val onRecyclerViewScrollListener = OnRecyclerViewScrollListener(this)
    val isProgressVisible = MutableLiveData<Boolean>().apply {
        value = true
    }

    fun onStart() {
        if (pageInfo == null) {
            loadNextPage()
        }
    }

    override fun isNextPageExists() = pageInfo?.next?.isNotBlank() ?: false

    override fun isNextPageLoading() = isPageLoading

    override fun loadNextPage() {
        if (!isPageLoading) {
            isPageLoading = true
            viewModelScope.launch(errorHandler) {
                try {
                    val response = if (pageInfo == null) {
                        ApiManager.loadFirstPageOfLocations()
                    } else {
                        ApiManager.loadNextPageOfLocations(pageInfo!!.next)
                    }
                    postData(response)
                } finally {
                    isPageLoading = false
                    isProgressVisible.postValue(false)
                }
            }
        }
    }

    @Throws(ResponseException::class)
    private fun postData(response: Response<LocationResponseBody>) {
        val responseBody = response.body()
        if (response.isSuccessful) {
            pageInfo = responseBody?.info ?: throw ResponseException(response.message())
            adapter.addData(responseBody.results)
        } else throw ResponseException(response.message())
    }

}
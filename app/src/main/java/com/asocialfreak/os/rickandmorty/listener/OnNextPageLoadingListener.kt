package com.asocialfreak.os.rickandmorty.listener

interface OnNextPageLoadingListener {
    fun isNextPageExists(): Boolean
    fun isNextPageLoading(): Boolean
    fun loadNextPage()
}
package com.asocialfreak.os.rickandmorty.view.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.lifecycle.Lifecycle
import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry
import androidx.recyclerview.widget.RecyclerView
import com.asocialfreak.os.rickandmorty.databinding.FragmentLocationsItemBinding
import com.asocialfreak.os.rickandmorty.model.data.Location
import com.asocialfreak.os.rickandmorty.view.viewmodel.LocationsItemViewModel

class LocationsRecyclerViewAdapter : RecyclerView.Adapter<LocationsRecyclerViewAdapter.ViewHolder>() {

    private var locations: MutableList<Location> = mutableListOf()

    fun addData(locations: Array<Location>) {
        this.locations.addAll(locations)
        notifyItemRangeInserted(this.locations.size, locations.size)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int) =
        LayoutInflater.from(parent.context).run {
            FragmentLocationsItemBinding.inflate(this, parent, false).let { binding ->
                val viewHolder = ViewHolder(binding.root)
                binding.viewModel = viewHolder.viewModel
                binding.lifecycleOwner = viewHolder
                viewHolder
            }
        }

    override fun getItemCount() = locations.size

    override fun onBindViewHolder(holder: ViewHolder, position: Int) =
        holder.bind(locations[position])


    inner class ViewHolder(view: View) : RecyclerView.ViewHolder(view), LifecycleOwner {

        val viewModel = LocationsItemViewModel()

        private val lifecycleRegistry = LifecycleRegistry(this)

        init {
            lifecycleRegistry.markState(Lifecycle.State.STARTED)
        }

        fun bind(location: Location) {
            viewModel.bindLocation(location)
        }

        override fun getLifecycle() = lifecycleRegistry
    }

}
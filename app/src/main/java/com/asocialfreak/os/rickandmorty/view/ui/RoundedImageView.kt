package com.asocialfreak.os.rickandmorty.view.ui

import android.content.Context
import android.graphics.*
import android.util.AttributeSet
import android.widget.ImageView
import android.graphics.drawable.Drawable
import android.net.Uri
import androidx.annotation.DrawableRes
import androidx.annotation.Nullable
import android.graphics.drawable.BitmapDrawable
import android.graphics.Bitmap
import android.graphics.Shader
import android.graphics.BitmapShader
import android.graphics.RectF
import android.graphics.Paint.ANTI_ALIAS_FLAG


class RoundedImageView: ImageView {

    private var bitmapShader: Shader? = null
    private val shaderMatrix: Matrix = Matrix()

    private val bitmapDrawBounds: RectF = RectF()

    private var bitmap: Bitmap? = null

    private var bitmapPaint: Paint = Paint(ANTI_ALIAS_FLAG)


    constructor(context: Context): this(context, null)
    constructor(context: Context, attrs: AttributeSet?): super(context, attrs) {
        loadBitmap()
    }

    override fun setImageResource(@DrawableRes resId: Int) {
        super.setImageResource(resId)
        loadBitmap()
    }

    override fun setImageDrawable(@Nullable drawable: Drawable?) {
        super.setImageDrawable(drawable)
        loadBitmap()
    }

    override fun setImageBitmap(@Nullable bm: Bitmap) {
        super.setImageBitmap(bm)
        loadBitmap()
    }

    override fun setImageURI(@Nullable uri: Uri) {
        super.setImageURI(uri)
        loadBitmap()
    }

    override fun onSizeChanged(w: Int, h: Int, oldw: Int, oldh: Int) {
        super.onSizeChanged(w, h, oldw, oldh)
        updateCircleDrawBounds(bitmapDrawBounds)
        updateBitmapSize()
    }


    override fun onDraw(canvas: Canvas) {
        drawBitmap(canvas)
    }

    private fun drawBitmap(canvas: Canvas) {
        canvas.drawOval(bitmapDrawBounds, bitmapPaint)
    }

    private fun updateCircleDrawBounds(bounds: RectF?) {
        val contentWidth = (width - paddingLeft - paddingRight).toFloat()
        val contentHeight = (height - paddingTop - paddingBottom).toFloat()

        var left = paddingLeft.toFloat()
        var top = paddingTop.toFloat()
        if (contentWidth > contentHeight) {
            left += (contentWidth - contentHeight) / 2f
        } else {
            top += (contentHeight - contentWidth) / 2f
        }

        val diameter = Math.min(contentWidth, contentHeight)
        bounds?.set(left, top, left + diameter, top + diameter)
    }

    private fun loadBitmap() {
        bitmap = getBitmapFromDrawable(drawable)
        if (bitmap == null) {
            return
        }

        bitmapShader = BitmapShader(bitmap!!, Shader.TileMode.CLAMP, Shader.TileMode.CLAMP)
        bitmapPaint.shader = bitmapShader

        updateBitmapSize()
    }

    private fun updateBitmapSize() {
        if (bitmap == null) return

        val dx: Float
        val dy: Float
        val scale: Float

        if (bitmap?.width!! < bitmap?.height!!) {
            scale = (bitmapDrawBounds.width() / bitmap?.width!!)
            dx = bitmapDrawBounds.left
            dy = bitmapDrawBounds.top - bitmap?.height!! * scale / 2f + bitmapDrawBounds.width() / 2f
        } else {
            scale = (bitmapDrawBounds.height() / bitmap?.height!!)
            dx = bitmapDrawBounds.left - bitmap?.width!! * scale / 2f + bitmapDrawBounds.width() / 2f
            dy = bitmapDrawBounds.top
        }
        shaderMatrix.setScale(scale, scale)
        shaderMatrix.postTranslate(dx, dy)
        bitmapShader?.setLocalMatrix(shaderMatrix)
    }

    private fun getBitmapFromDrawable(drawable: Drawable?): Bitmap? {
        if (drawable == null) {
            return null
        }

        if (drawable is BitmapDrawable) {
            return drawable.bitmap
        }

        val bitmap = Bitmap.createBitmap(
            drawable.intrinsicWidth,
            drawable.intrinsicHeight,
            Bitmap.Config.ARGB_8888
        )
        val canvas = Canvas(bitmap)
        drawable.setBounds(0, 0, canvas.width, canvas.height)
        drawable.draw(canvas)

        return bitmap
    }


}
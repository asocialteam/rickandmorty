package com.asocialfreak.os.rickandmorty.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.asocialfreak.os.rickandmorty.R
import com.asocialfreak.os.rickandmorty.databinding.ActivityLocationDetailBinding
import com.asocialfreak.os.rickandmorty.view.viewmodel.LocationDetailActivityViewModel

const val KEY_INTENT_LOCATION = "key_episode"

class LocationDetailActivity : AppCompatActivity() {

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(LocationDetailActivityViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onCreate(intent)
        DataBindingUtil.setContentView<ActivityLocationDetailBinding>(
            this,
            R.layout.activity_location_detail
        ).also { binding ->
            binding.viewModel = viewModel
        }
    }

    override fun onStart() {
        super.onStart()
        viewModel.onStart()
    }

}

package com.asocialfreak.os.rickandmorty.api.service

import com.asocialfreak.os.rickandmorty.model.EpisodeResponseBody
import com.asocialfreak.os.rickandmorty.model.data.Episode
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Url

interface EpisodeService {
    @GET("episode")
    fun loadEpisodesAsync(): Deferred<Response<EpisodeResponseBody>>

    @GET("episode/{episodeIds}")
    fun loadEpisodesByIdsAsync(@Path("episodeIds") episodeIds: String): Deferred<Response<Array<Episode>>>

    @GET("episode/{episodeId}")
    fun loadEpisodeByIdAsync(@Path("episodeId") episodeId: String): Deferred<Response<Episode>>

    @GET
    fun loadNextPageAsync(@Url url: String): Deferred<Response<EpisodeResponseBody>>
}
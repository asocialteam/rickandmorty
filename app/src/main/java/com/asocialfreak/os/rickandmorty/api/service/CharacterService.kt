package com.asocialfreak.os.rickandmorty.api.service

import com.asocialfreak.os.rickandmorty.model.data.Character
import com.asocialfreak.os.rickandmorty.model.CharacterResponseBody
import kotlinx.coroutines.Deferred
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Url

interface CharacterService {
    @GET("character")
    fun loadCharacterAsync(): Deferred<Response<CharacterResponseBody>>

    @GET
    fun loadNextPageAsync(@Url url: String): Deferred<Response<CharacterResponseBody>>

    @GET("character/{characterIds}")
    fun loadCharactersByIdsAsync(@Path("characterIds") characterIds: String): Deferred<Response<Array<Character>>>

    @GET("character/{characterId}")
    fun loadCharacterByIdAsync(@Path("characterId") characterId: String): Deferred<Response<Character>>
}
package com.asocialfreak.os.rickandmorty.view.viewmodel

import android.content.Intent
import android.util.Log
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import com.asocialfreak.os.rickandmorty.view.KEY_INTENT_EPISODE
import com.asocialfreak.os.rickandmorty.api.ApiManager
import com.asocialfreak.os.rickandmorty.model.ResponseException
import com.asocialfreak.os.rickandmorty.model.data.Episode
import com.asocialfreak.os.rickandmorty.view.adapter.CharactersRecyclerViewAdapter
import kotlinx.coroutines.*

class EpisodeDetailActivityViewModel : ViewModel() {

    companion object {
        private const val TAG = "EpisodeDetailAcVM"
    }

    private val parentJob = Job()
    private val viewModelScope = CoroutineScope(parentJob + Dispatchers.Default)
    private val errorHandler
        get() = CoroutineExceptionHandler { _, e ->
            when (e) {
                is ResponseException -> Log.e(TAG, "Error while processing a response", e)
                else -> Log.e(TAG, "General error occurred", e)
            }
            areCharactersLoaded = false
        }

    private var areCharactersLoaded = false
    private var episode: Episode? = null

    val liveEpisode = LiveEpisode()
    val adapter = CharactersRecyclerViewAdapter()

    fun onCreate(intent: Intent) {
        if (this.episode == null) {
            this.episode = intent.getParcelableExtra(KEY_INTENT_EPISODE) ?: return
            liveEpisode.apply {
                name.value = episode!!.name
                episodeOrder.value = episode!!.episode
                airDate.value = episode!!.air_date.toLowerCase()
            }
        }
    }

    fun onStart() {
        if (!areCharactersLoaded && episode != null) {
            areCharactersLoaded = true
            loadCharacters(episode!!.characters)
        }
    }

    private fun loadCharacters(characterUrls: Array<String>) {
        if (characterUrls.isNotEmpty()) {
            viewModelScope.launch(errorHandler) {
                val characterIds = characterUrls.map { episode ->
                    episode.split('/').last()
                }

                val characters = ApiManager.loadCharactersByIds(characterIds)
                adapter.addData(characters)
            }
        }
    }

    inner class LiveEpisode {
        val name = MutableLiveData<String>()
        val episodeOrder = MutableLiveData<String>()
        val airDate = MutableLiveData<String>()
    }

}
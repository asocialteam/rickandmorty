package com.asocialfreak.os.rickandmorty.model

import java.lang.Exception

class ResponseException(message: String): Exception(message)
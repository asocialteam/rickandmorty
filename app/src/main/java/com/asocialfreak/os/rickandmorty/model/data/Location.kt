package com.asocialfreak.os.rickandmorty.model.data

import android.os.Parcel
import android.os.Parcelable

class Location() : Parcelable{
    var id: Long = 0
    var name: String = ""
    var type: String = ""
    var dimension: String = ""
    var residents: Array<String> = emptyArray()

    constructor(parcel: Parcel) : this() {
        id = parcel.readLong()
        name = parcel.readString() ?: ""
        type = parcel.readString() ?: ""
        dimension = parcel.readString() ?: ""
        residents = parcel.createStringArray() ?: emptyArray()
    }

    override fun writeToParcel(parcel: Parcel, flags: Int) {
        parcel.writeLong(id)
        parcel.writeString(name)
        parcel.writeString(type)
        parcel.writeString(dimension)
        parcel.writeStringArray(residents)
    }

    override fun describeContents(): Int {
        return 0
    }

    companion object CREATOR : Parcelable.Creator<Location> {
        override fun createFromParcel(parcel: Parcel): Location {
            return Location(parcel)
        }

        override fun newArray(size: Int): Array<Location?> {
            return arrayOfNulls(size)
        }
    }

}
package com.asocialfreak.os.rickandmorty.view

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.databinding.DataBindingUtil
import androidx.lifecycle.ViewModelProviders
import com.asocialfreak.os.rickandmorty.R
import com.asocialfreak.os.rickandmorty.databinding.ActivityMainBinding
import com.asocialfreak.os.rickandmorty.view.viewmodel.MainActivityViewModel

class MainActivity : AppCompatActivity() {

    private val viewModel by lazy {
        ViewModelProviders.of(this).get(MainActivityViewModel::class.java)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        viewModel.onCreate(this, supportFragmentManager)
        DataBindingUtil.setContentView<ActivityMainBinding>(
            this,
            R.layout.activity_main
        ).also { binding ->
            binding.viewModel = viewModel
        }
    }
}